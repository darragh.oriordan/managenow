import * as React from "react";
import { Link } from "react-router-dom";
import {
  Button,
  ButtonProps,
  Card,
  Grid,
  GridColumn,
  Icon,
  Image,
  Popup,
  Rating
} from "semantic-ui-react";
import { TechniqueSourceType } from "../../models/Enums";
import ICategory from "../../models/ICategory";
import ITechnique from "../../models/ITechnique";
import { getCategories } from "../../services/techniqueService";
import "./DevelopmentItemCards.css";
interface IDevelopmentItemProps {
  techniques: ITechnique[];
  onAssignClick: () => void;
}
export default class DevelpmentItems extends React.Component<
  IDevelopmentItemProps,
  any
> {
  public static defaultProps: Partial<IDevelopmentItemProps> = {
    techniques: [] as ITechnique[]
  };

  constructor(props: IDevelopmentItemProps) {
    super(props);
  }

  public render() {
    return (
      <Grid doubling={true} stackable={true}>
        {this.props.techniques.map((technique: ITechnique, ind: number) => (
          <GridColumn
            key={ind}
            widescreen={2}
            largeScreen={3}
            computer={4}
            mobile={16}
            tablet={8}
          >
            <Card key={technique.id} raised={true}>
              {technique.coverimage && (
                <Image src={technique.coverimage} style={{ width: "100%" }} />
              )}
              <Card.Content>
                <Card.Header className="development-item-cards__technique-name">
                  {technique.name}
                </Card.Header>
                <Card.Meta className="development-item-cards__source-category">
                  <Link to={"/category/" + technique.category.toLowerCase()}>
                    #{
                      (
                        getCategories().find(
                          (cat: ICategory) =>
                            cat.safeName === technique.category
                        ) || ({} as ICategory)
                      ).humanizedName
                    }
                  </Link>
                </Card.Meta>
                {technique.type === TechniqueSourceType.book && (
                  <Card.Meta className="development-item-cards__source-name-and-author">
                    <span className="development-item-cards__source-name">
                      {technique.sourcename}
                    </span>{" "}
                    by{" "}
                    <span className="development-item-cards__source-author">
                      {technique.author}
                    </span>
                  </Card.Meta>
                )}
                {technique.type !== TechniqueSourceType.book && (
                  <Card.Meta className="development-item-cards__source-name-and-author">
                    <span className="development-item-cards__source-name">
                      {technique.sourcename}
                    </span>
                  </Card.Meta>
                )}
                {technique.locationInSource && (
                  <Card.Meta className="development-item-cards__source-location">
                    Location: {technique.locationInSource}
                  </Card.Meta>
                )}

                <Card.Meta className="development-item-cards__rating">
                  <Rating
                    icon="star"
                    defaultRating={technique.rating / 2}
                    disabled={true}
                    maxRating={5}
                  />
                </Card.Meta>
                <Card.Description>{technique.description}</Card.Description>
                <Card.Content
                  extra={true}
                  className="development-item-cards__actions"
                >
                  <Button
                    content="Get"
                    icon="linkify"
                    labelPosition="left"
                    style={{ marginBottom: "1em" }}
                    fluid={true}
                    // tslint:disable-next-line:jsx-no-lambda
                    onClick={(e: any, data: ButtonProps) => {
                      const win = window.open(technique.referralLink, "_blank");
                      (win || ({} as Window)).focus();
                    }}
                  />
                  <Popup
                    trigger={
                      <Button
                        type="button"
                        color="orange"
                        fluid={true}
                        // tslint:disable-next-line:jsx-no-lambda
                        onClick={(e: any, data: ButtonProps) => {
                          this.props.onAssignClick();
                        }}
                      >
                        <Icon className="lock" />Assign
                        <Icon className="right chevron" />
                      </Button>
                    }
                    content="With the app you can assign techniques and track progress."
                    on="hover"
                  />
                </Card.Content>
              </Card.Content>
            </Card>
          </GridColumn>
        ))}
      </Grid>
    );
  }
}
