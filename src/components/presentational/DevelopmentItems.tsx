import * as React from "react";
import {
  Button,
  ButtonProps,
  Icon,
  Item,
  Popup,
  Rating
} from "semantic-ui-react";
import ITechnique from "../../models/ITechnique";

interface IDevelopmentItemProps {
  techniques: ITechnique[];
  onAssignClick: () => void;
}
export default class DevelpmentItems extends React.Component<
  IDevelopmentItemProps,
  any
> {
  constructor(props: IDevelopmentItemProps) {
    super(props);
  }

  public render() {
    return (
      <Item.Group divided={true}>
        {this.props.techniques.map(technique => (
          <Item key={technique.id}>
            {technique.coverimage && (
              <Item.Image size="tiny" src={technique.coverimage} />
            )}
            <Item.Content>
              <Item.Header>{technique.name}</Item.Header>
              <Item.Meta>
                <span className="source-name">
                  Source: {technique.sourcename}
                </span>{" "}
                by <span className="source-author">{technique.author}</span>
              </Item.Meta>
              <Item.Meta>
                <span className="source-location">
                  Location: {technique.locationInSource}
                </span>
              </Item.Meta>

              <Item.Meta>
                <Rating
                  icon="star"
                  defaultRating={technique.rating / 2}
                  disabled={true}
                  maxRating={5}
                />
              </Item.Meta>
              <Item.Description>{technique.description}</Item.Description>
              <Item.Extra>
                <Button
                  content="Get this source (external site)"
                  icon="linkify"
                  labelPosition="left"
                  floated="left"
                  // tslint:disable-next-line:jsx-no-lambda
                  onClick={(e: any, data: ButtonProps) => {
                    const win = window.open(technique.referralLink, "_blank");
                    (win || ({} as Window)).focus();
                  }}
                />
                <Popup
                  trigger={
                    <Button
                      type="button"
                      color="orange"
                      floated="right"
                      // tslint:disable-next-line:jsx-no-lambda
                      onClick={(e: any, data: ButtonProps) => {
                        this.props.onAssignClick();
                      }}
                    >
                      <Icon className="lock" />Assign task to a team member
                      <Icon className="right chevron" />
                    </Button>
                  }
                  content="With the app you can assign techniques and track progress."
                  on="hover"
                />
              </Item.Extra>
            </Item.Content>
          </Item>
        ))}
      </Item.Group>
    );
  }
}
