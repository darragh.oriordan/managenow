import * as React from "react";
import { Dropdown } from "semantic-ui-react";
import ICategory from "../../models/ICategory";
import { getCategories } from "../../services/techniqueService";

interface IDropdownOption {
  key: string;
  value: string;
  text: string;
}

interface ITechniqueCategorySelectorProps {
  categories: {};
  placeholderText: string;
  onSelectChanged: (event: any, data: any) => void;
}

export class TechniqueCategorySelector extends React.PureComponent<
  ITechniqueCategorySelectorProps,
  any
> {
  constructor(props: ITechniqueCategorySelectorProps) {
    super(props);
  }
  public getCategoriesAsDropDownModels(categories: object): IDropdownOption[] {
    return getCategories().map((cat: ICategory) => {
      return {
        key: cat.safeName,
        text: cat.humanizedName,
        value: cat.safeName
      };
    });
  }

  public render() {
    return (
      <div>
        <span>Filter </span>
        <Dropdown
          placeholder={this.props.placeholderText}
          fluid={true}
          search={true}
          selection={true}
          options={this.getCategoriesAsDropDownModels(this.props.categories)}
          onChange={this.props.onSelectChanged}
        />
      </div>
    );
  }
}

export default TechniqueCategorySelector;
