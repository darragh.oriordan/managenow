import * as React from "react";
import { Link, RouteComponentProps } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { Container, Dropdown, Icon, Menu } from "semantic-ui-react";
import constants from "../../constants/constants";

interface IHeaderMenuProps extends RouteComponentProps<any> {
  displayName?: string;
  onSignOut: (history: any) => void;
  authenticated: boolean;
}

class HeaderMenu extends React.PureComponent<IHeaderMenuProps, any> {
  public static defaultProps: Partial<IHeaderMenuProps> = {
    displayName: ""
  };
  public render() {
    return (
      <Menu fixed="top">
        <Container>
          <Menu.Item as={Link} header={true} to={constants.ROUTES.HOME}>
            {/* <Image
              size="mini"
              src="./logo.png"
              style={{ marginRight: "1.5em" }}
            /> */}
            {constants.APP_NAME}
          </Menu.Item>
          <Menu.Item as={Link} to={constants.ROUTES.LIST_CATEGORIES}>
            Categories
          </Menu.Item>
          <Menu.Item as={Link} to={constants.ROUTES.APP_SALES}>
            The App
          </Menu.Item>
          {this.props.authenticated && (
            <Menu.Item as={Link} to={constants.ROUTES.LANDING}>
              Your Team
            </Menu.Item>
          )}
          <Menu.Menu position="right">
            {!this.props.authenticated && (
              <Menu.Item as={Link} to={constants.ROUTES.SIGN_UP}>
                <span style={{ color: "#3ea9f5" }}>
                  {" "}
                  <Icon name="user outline" /> Sign Up
                </span>
              </Menu.Item>
            )}
            {!this.props.authenticated && (
              <Menu.Item as={Link} to={constants.ROUTES.SIGN_IN}>
                <Icon name="sign in" /> Login
              </Menu.Item>
            )}
            {this.props.authenticated && (
              <Dropdown item={true} text={this.props.displayName}>
                <Dropdown.Menu>
                  <Dropdown.Item
                    icon="edit"
                    text="Signout"
                    // tslint:disable-next-line:jsx-no-lambda
                    onClick={() => this.props.onSignOut(this.props.history)}
                  />
                </Dropdown.Menu>
              </Dropdown>
            )}
          </Menu.Menu>
        </Container>
      </Menu>
    );
  }
}

export default withRouter(HeaderMenu);
