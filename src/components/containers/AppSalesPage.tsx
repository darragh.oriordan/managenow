import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { Button, Header, Icon } from "semantic-ui-react";
import constants from "../../constants/constants";
import "./AppSalesPage.css";

class AppSalesPage extends React.Component<RouteComponentProps<any>, any> {
  constructor(props: RouteComponentProps<any>) {
    super(props);
    this.onStartForFreeSelected = this.onStartForFreeSelected.bind(this);
    this.onViewListSelected = this.onViewListSelected.bind(this);
    
  }
  public onStartForFreeSelected() {
    this.props.history.push(constants.ROUTES.SIGN_UP);
  }
  public onViewListSelected() {
    this.props.history.push(constants.ROUTES.OPEN_LIST);
  }
  public render() {
    return (
      <div className="app-sales-page">
        <div className="app-sales-page__header-hero">
          <div className="app-sales-page__header-hero--bg">&nbsp;</div>
          <div className="app-sales-page__header-hero--message">
            <Header as="h1" size="huge">
              <span className="white">Leadership software</span> for <br />
              the people-first leader
            </Header>
            <Button
              className="app-sales-page__header-hero--button"
              size="massive"
              content="Start for free"
              basic={true}
              onClick={
                // tslint:disable-next-line:jsx-no-lambda
                () => this.onStartForFreeSelected()
              }
            />
          </div>
          <div className="app-sales-page__header-hero--image">
            <img src="/images/giraffe.jpg" height="500px" />
          </div>
        </div>
        <div className="app-sales-page__cta-bricks--wrap">
          <div className="app-sales-page__cta-bricks">
            <div className="app-sales-page__brick">
              <div className="app-sales-page__brick--icon">
                <span className="white">
                  {" "}
                  <Icon name="flask" size="huge" />
                </span>
              </div>
              <h2 className="app-sales-page__brick--title">
                Mentoring: Instantly access relevant techniques
              </h2>
              <div className="app-sales-page__brick--message">
                Improve your team with instant references to content from your
                favorite leadership books, blogs and podcasts.{" "}
              </div>
              <Button
                className="app-sales-page__brick--button"
                size="large"
                content="Start for free"
                basic={true}
                onClick={
                  // tslint:disable-next-line:jsx-no-lambda
                  () => this.onStartForFreeSelected()
                }
              />
            </div>
            <div className="app-sales-page__brick">
              <div className="app-sales-page__brick--icon">
                <span className="white">
                  {" "}
                  <Icon name="clock" size="huge" />
                </span>
              </div>
              <h2 className="app-sales-page__brick--title">
                Engagement: Reminders to check in on your team members regularly
              </h2>
              <div className="app-sales-page__brick--message">
                Use AI and machine learning to monitor the happiness of your
                team and get ideas on how to engage them.{" "}
              </div>
              <Button
                className="app-sales-page__brick--button"
                size="large"
                content="Start for free"
                basic={true}
                onClick={
                  // tslint:disable-next-line:jsx-no-lambda
                  () => this.onStartForFreeSelected()
                }
              />
            </div>
            <div className="app-sales-page__brick">
              <div className="app-sales-page__brick--icon">
                <span className="white">
                  {" "}
                  <Icon name="check" size="huge" />
                </span>
              </div>
              <h2 className="app-sales-page__brick--title">
                Tasks: For you and your team. Never forget a task again!
              </h2>
              <div className="app-sales-page__brick--message">
                Effectively track those tasks that surface throughout the day -
                on mobile and in the cloud.{" "}
              </div>
              <Button
                className="app-sales-page__brick--button"
                size="large"
                content="Start for free"
                basic={true}
                onClick={
                  // tslint:disable-next-line:jsx-no-lambda
                  () => this.onStartForFreeSelected()
                }
              />
            </div>
          </div>
        </div>
        <div className="app-sales-page__list-cta">
          <Header as="h1" size="huge">
              Only here for the list?
            </Header>
            <Header as="h3">
                Find solutions to your leadership problems right now
            </Header>
            <Button            
              size="massive"
              content="View the New Leader List"
              basic={true}
              onClick={
                // tslint:disable-next-line:jsx-no-lambda
                () => this.onViewListSelected()
              }
            />
        </div>
      </div>
    );
  }
}
export default withRouter(AppSalesPage);
