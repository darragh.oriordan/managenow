import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { Header } from "semantic-ui-react";
import constants from "../../constants/constants";
import ITechnique from "../../models/ITechnique";
import DevelopmentItemCards from "../presentational/DevelopmentItemCards";

interface IOpenListPageProps extends RouteComponentProps<any> {
  techniques: ITechnique[];
  header:string;
}

class OpenListPage extends React.Component<
  IOpenListPageProps,
  any
> {
  constructor(props: IOpenListPageProps) {
    super(props);
    this.onAssignClick = this.onAssignClick.bind(this);
  }

  public onAssignClick() {
    this.props.history.push(constants.ROUTES.SIGN_UP);
  }

  public render() {
    return (
      <div
        style={{
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "5em",
          width: "90%"
        }}
      >
        <Header as="h1" size="huge">{this.props.header}</Header>
        <DevelopmentItemCards
          onAssignClick={this.onAssignClick}
          techniques={this.props.techniques}
        />
      </div>
    );
  }
}

export default withRouter(OpenListPage);
