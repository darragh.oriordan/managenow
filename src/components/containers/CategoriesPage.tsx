import * as React from "react";
import { RouteComponentProps, withRouter } from "react-router";
import { Button, Header } from "semantic-ui-react";
import ICategory from "../../models/ICategory";
import { getCategories } from "../../services/techniqueService";

class CategoriesPage extends React.Component<RouteComponentProps<any>, any> {
  constructor(props: RouteComponentProps<any>) {
    super(props);
  }
  public gotoCategory(category: string) {
    this.props.history.push("/category/" + category);
  }
  public render() {
    return (
      <div
        style={{
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "5em",
          width: "90%"
        }}
      >
        <Header as="h1" size="huge">
          Categories
        </Header>
        {getCategories().map((cat: ICategory) => {
          return (
            <Button
            style={{margin:".2em"}}
            basic={true}
              key={cat.safeName}
              // tslint:disable-next-line:jsx-no-lambda
              onClick={() => this.gotoCategory(cat.safeName)}
            >
              {cat.humanizedName}
            </Button>
          );
        })}
      </div>
    );
  }
}

export default withRouter(CategoriesPage);
