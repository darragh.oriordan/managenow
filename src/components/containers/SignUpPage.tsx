import * as React from "react";
import { FirebaseAuth } from "react-firebaseui";
import { Redirect, RouteComponentProps, withRouter } from "react-router-dom";
import { Grid, Header } from "semantic-ui-react";
import { auth, uiConfig } from "../../api/firebase";
import constants from "../../constants/constants";
import "./SignUpPage.css";

interface ISignInPageProps extends RouteComponentProps<any> {
  authenticated: boolean;
}
interface ISignInPageState {
  loading: boolean;
}

class SignUpPage extends React.Component<ISignInPageProps, ISignInPageState> {
  constructor(props: ISignInPageProps) {
    super(props);
    this.state = {
      loading: true
    };
  }
  public componentDidMount() {
    this.setState({ loading: false });
  }

  public render() {
    if (this.props.authenticated) {
      return <Redirect to={constants.ROUTES.LANDING} />;
    }

    if (this.state.loading) {
      return <div>loading...</div>;
    }

    return (
      <div className="signup-page">
        <Grid stackable={true}>
          <Grid.Row>
            <Grid.Column width={4} />
            <Grid.Column width={8}>
              <div className="signup-page__form-container">
                <h1>Become a better people leader today</h1>
                <Header as="h3">
                  <span className="white">
                    {" "}
                    Easily find the techniques you need to solve specific leadership problems and get the tools
                    to support you and your team.
                  </span>
                </Header>

                <FirebaseAuth uiConfig={uiConfig} firebaseAuth={auth} />
              </div>
            </Grid.Column>
            <Grid.Column width={4} />
          </Grid.Row>

          <Grid.Row
            style={{ paddingTop: "4em" }}
            className="signup-page__faq-container"
          >
            <Grid.Column width={2} />
            <Grid.Column width={6}>
              <h4>Is there a catch?</h4>
              No, it's free for now but we would love some feedback.
            </Grid.Column>
            <Grid.Column width={6}>
              <h4>Do I have to worry about you selling my email or data?</h4>
              No. We won't do that
            </Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>

          <Grid.Row className="signup-page__faq-container">
            <Grid.Column width={2} />
            <Grid.Column width={6}>
              <h4>I have other questions! How can I get in touch?</h4>
              Give me a shout on twitter @darraghor
            </Grid.Column>
            <Grid.Column width={6}>&nbsp;</Grid.Column>
            <Grid.Column width={2} />
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default withRouter(SignUpPage);
