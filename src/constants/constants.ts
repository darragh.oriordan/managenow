const constants = {
  APP_NAME: "New Leader List",
  FIELD_STRINGS: {
    openListtechniqueSearchPlaceholderText: "Choose a topic...",
    techniqueSearchPlaceholderText: "We will work on..."
  },
  ROUTES: {
    ABOUT: "/about",
    ACCOUNT: "/account",
    APP_SALES: "/app",
    HOME: "/",
    LANDING: "/team",
    LIST_AUTHORS: "/authors",
    LIST_CATEGORIES: "/categories",
    LIST_CATEGORY: "/category/:category",
    OPEN_LIST: "/list",
    SIGN_IN: "/login",
    SIGN_UP: "/signup",
    TEAM_MEMBER_ADD: "/team/add",
    TEAM_MEMBER_DEV_TASK_ADD: "/team/member/:id/devtasks/add",
    TEAM_MEMBER_DEV_TASK_OVERVIEW: "/team/member/:id/devtasks",
    TEAM_MEMBER_INTERACTION_ADD: "/team/member/:id/interactions/add",
    TEAM_MEMBER_INTERACTION_OVERVIEW: "/team/member/:id/interactions",
    TEAM_MEMBER_OVERVIEW: "/team/member/:id",
    TEAM_MEMBER_TODOS_ADD: "/team/member/:id/todos/add",
    TEAM_MEMBER_TODOS_OVERVIEW: "/team/member/:id/todos"
  },
  TECHNIQUE_CATEGORY_KEYS: {
    BuildingCulture: "building+culture",
    BuildingProducts: "building+products",
    BuildingRapportandInfluence: "building+rapport+and+influence",
    Coaching: "coaching",
    Decisions: "decisions",
    Delegating: "delegating",
    DirectingPeople: "directing+people",
    Diversity: "diversity",
    FacilitatingMeetings: "facilitating+meetings",
    Firing: "firing",
    GivingFeedback: "giving+feedback",
    Hiring: "hiring",
    LeaershipSkills: "leadership+skills+and+abilities",
    Listening: "listening",
    ManagingConflict: "managing+conflict",
    ManagingManagers: "managing+managers",
    Mentoring: "mentoring",
    Motivation: "motivation",
    NewLeader:"new+leader",
    Onboarding: "onboarding",
    PerformanceReviews: "performance+reviews",
    RecognisingAndRewarding: "recognising+and+rewarding",
    Retention: "retention",
    SettingGoals: "setting+goals",
    Teamwork: "teamwork"
  }
};

export default constants;
