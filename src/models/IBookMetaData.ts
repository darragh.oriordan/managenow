import { TechniqueSourceType } from "./Enums";

export default interface IBookMetaData {
    author: string;
    coverimage: string;
    sourcename: string;
    type: TechniqueSourceType;
    referralLink: string;
  }