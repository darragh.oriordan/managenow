export default interface ICategory{
    safeName:string;
    humanizedName:string;
}