import ICategory from "../models/ICategory";
import ITechnique from "../models/ITechnique";
import * as techniques from "./techniques";
export function getRelevantTechniques(behaviourKey: string): ITechnique[] {
  if (!behaviourKey) {
    return getTechniques();
  }
  return getTechniques().filter(element => element.category === behaviourKey);
}
export function getTechniques(): ITechnique[] {
  const sampleTechniques = new Array<ITechnique>();
  sampleTechniques.push(...techniques.management30());
  sampleTechniques.push(...techniques.blogposts());
  sampleTechniques.push(...techniques.videos());
  sampleTechniques.push(...techniques.thefirst90days());
  sampleTechniques.push(...techniques.howtobeagreatboss());

  return sampleTechniques.map((technique: ITechnique, index: number) => {
    technique.id = index.toString();
    return technique;
  });
}

export function getCategories(): ICategory[] {
  return [
    { safeName: "building+culture", humanizedName: "Building Culture" },
    { safeName: "building+products", humanizedName: "Building Products" },
    {
      humanizedName: "Building Rapport and Influence",
      safeName: "building+rapport+and+influence"
    },
    { safeName: "coaching", humanizedName: "Coaching" },
    { safeName: "decisions", humanizedName: "Decisions" },
    { safeName: "delegating", humanizedName: "Delegating" },
    { safeName: "directing+people", humanizedName: "Directing People" },
    { safeName: "diversity", humanizedName: "Diversity" },
    {
      humanizedName: "Facilitating Meetings",
      safeName: "facilitating+meetings"
    },
    { safeName: "firing", humanizedName: "Firing" },
    { safeName: "giving+feedback", humanizedName: "Giving Feedback" },
    { safeName: "hiring", humanizedName: "Hiring" },
    {
      humanizedName: "Leadership skills and abilities",
      safeName: "leadership+skills+and+abilities"
    },
    { safeName: "listening", humanizedName: "Listening" },
    { safeName: "managing+conflict", humanizedName: "Managing Conflict" },
    { safeName: "managing+managers", humanizedName: "Managing Managers" },
    { safeName: "mentoring", humanizedName: "Mentoring" },
    { safeName: "motivation", humanizedName: "Motivation" },
    { safeName: "new+leader", humanizedName: "New Leader" },
    { safeName: "onboarding", humanizedName: "Onboarding" },
    { safeName: "performance+reviews", humanizedName: "Performance Reviews" },
    {
      humanizedName: "Recognising and Rewarding",
      safeName: "recognising+and+rewarding"
    },
    { safeName: "retention", humanizedName: "Retention" },
    { safeName: "setting+goals", humanizedName: "Setting Goals" },
    { safeName: "teamwork", humanizedName: "Teamwork" }
  ];
}
