import constants from "../../constants/constants";
import { TechniqueSourceType } from "../../models/Enums";
import IBookMetaData from "../../models/IBookMetaData";
import ITechnique from "../../models/ITechnique";

export default function getTechniques(): ITechnique[] {
  const sampleTechniques = new Array<ITechnique>();
  const shared: IBookMetaData = {
    author: "Gino Wickman, René Boer",
    coverimage: "/images/covers/howtobeagreatboss.jpg",
    referralLink:
      "https://www.amazon.com/How-Great-Boss-Gino-Wickman/dp/1942952848",

    sourcename: "How to be a great boss",
    type: TechniqueSourceType.book
  };

  sampleTechniques.push({
    ...shared,
    // authorTwitter: "MichaelDWatkins",
    category: constants.TECHNIQUE_CATEGORY_KEYS.Delegating,

    description:
      "Learn how to list and categorize the tasks you perform daily as a manager so you can delegate the items that dont need your skills and time.",
    id: "203",
    locationInSource: "Page 24",
    name: "How to delegate",
    rating: 5
  });
  sampleTechniques.push({
    ...shared,
    // authorTwitter: "MichaelDWatkins",
    category: constants.TECHNIQUE_CATEGORY_KEYS.BuildingCulture,

    description:
      "Learn how to identify some leaders you respect so you can identify which values you care about.",
    id: "202",
    locationInSource: "Page 24",
    name: "Discover your core values",
    rating: 5
  });
  sampleTechniques.push({
    ...shared,
    // authorTwitter: "MichaelDWatkins",
    category: constants.TECHNIQUE_CATEGORY_KEYS.BuildingRapportandInfluence,

    description:
      "Learn how to identify which members of your team are aligned with your values.",
    id: "201",
    locationInSource: "Page 43",
    name: "Identify team member alignment",
    rating: 5
  });
  sampleTechniques.push({
    ...shared,
    // authorTwitter: "MichaelDWatkins",
    category: constants.TECHNIQUE_CATEGORY_KEYS.GivingFeedback,

    description:
      "A model for people problems split in to four common types of problem",
    id: "200",
    locationInSource: "Chapter 9",
    name: "the four types of people problem",
    rating: 5
  });
  return sampleTechniques;
}
