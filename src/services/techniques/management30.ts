import constants from "../../constants/constants";
import { TechniqueSourceType } from "../../models/Enums";
import IBookMetaData from "../../models/IBookMetaData";
import ITechnique from "../../models/ITechnique";

export default function getTechniques(): ITechnique[] {
  const sampleTechniques = new Array<ITechnique>();
  const shared: IBookMetaData = {
    author: "Jurgen Appelo",
    coverimage: "/images/covers/management30.jpg",
    referralLink:
      "https://www.amazon.com/Management-3-0-Developers-Developing-Addison-Wesley/dp/0321712471/ref=sr_1_2?ie=UTF8&qid=1524119998&sr=8-2&keywords=management+3.0",

    sourcename: "Management 3.0",
    type: TechniqueSourceType.book
  };

  sampleTechniques.push({
    ...shared,
    category: constants.TECHNIQUE_CATEGORY_KEYS.BuildingProducts,
    description:
      "Discuss the identifiable results of innovation in our organisation. If we cant identify any then why not?",
    id: "1000",
    locationInSource: "Chapter 4. The Information-Innovation System",
    name: "Discuss Identifiable Innovation",
    rating: 8
  });

  return sampleTechniques;
}
