import constants from "../../constants/constants";
import { TechniqueSourceType } from "../../models/Enums";
import ITechnique from "../../models/ITechnique";

export default function getTechniques(): ITechnique[] {
  const sampleTechniques = new Array<ITechnique>();
  const type = TechniqueSourceType.video;
  const coverimage = "/images/covers/raffi-blog.jpg";
  sampleTechniques.push({
    author: "Keith Rabois",
    category: constants.TECHNIQUE_CATEGORY_KEYS.BuildingCulture,
    coverimage,
    description:
      "Great video from Keith Rabois on how to operate a company. This is directed at startup founders but has useful tips for people leadership for anyone.",
    id: "144",
    locationInSource:
      "",
    name: "How to Operate",
    rating: 10,
    referralLink:
      "http://startupclass.samaltman.com/courses/lec14/",
    sourcename: "http://startupclass.samaltman.com",
    type
  });

  return sampleTechniques;
}
