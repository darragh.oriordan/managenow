import constants from "../../constants/constants";
import { TechniqueSourceType } from "../../models/Enums";
import ITechnique from "../../models/ITechnique";

export default function getTechniques(): ITechnique[] {
  const sampleTechniques = new Array<ITechnique>();
  const type = TechniqueSourceType.blog;
  const coverimage = "/images/covers/raffi-blog.jpg";
  sampleTechniques.push({
    author: "Get Lighthouse",
    category: constants.TECHNIQUE_CATEGORY_KEYS.BuildingRapportandInfluence,
    coverimage,
    description:
      "How to start one on ones with your team including email templates, scheduling advice and how to follow up. Also includes links to lighthouse's awesome content on how to be a better manager.",
    id: "3009",
    locationInSource:
      "",
    name: "Starting one-on-ones",
    rating: 6,
    referralLink:
      "https://getlighthouse.com/blog/how-to-start-one-on-ones-your-teams/",
    sourcename: "https://getlighthouse.com/",
    type
  });
  sampleTechniques.push({
    author: "Keyvalues.com",
    category: constants.TECHNIQUE_CATEGORY_KEYS.BuildingCulture,
    coverimage,
    description:
      "The key values web site is used by engineers to find jobs based on culture. Audit your organisation and pick the top eight key values you would want people to think of when they think of your culture. Chat to your team in one on ones about what they feel the key values are and compare.",
    id: "2009",
    locationInSource: "",
    name: "List your organization's key values",
    rating: 8,
    referralLink: "https://www.keyvalues.com/",

    sourcename: "https://www.keyvalues.com/",
    type
  });

  return sampleTechniques;
}
