import constants from "../../constants/constants";
import { TechniqueSourceType } from "../../models/Enums";
import IBookMetaData from "../../models/IBookMetaData";
import ITechnique from "../../models/ITechnique";

export default function getTechniques(): ITechnique[] {
  const sampleTechniques = new Array<ITechnique>();
  const shared: IBookMetaData = {
    author: "Michael D. Watkins",
    coverimage: "/images/covers/thefirst90days.jpg",
    referralLink:
      "https://www.amazon.com/First-Days-Updated-Expanded-Strategies-ebook/dp/B00B6U63ZE/ref=sr_1_4?s=books&ie=UTF8&qid=1524128886&sr=1-4&keywords=first+90+days",

    sourcename: "The First 90 Days",
    type: TechniqueSourceType.book
  };

  sampleTechniques.push({
    ...shared,
    // authorTwitter: "MichaelDWatkins",
    category: constants.TECHNIQUE_CATEGORY_KEYS.NewLeader,

    description:
      "Learn how to overcome the barriers for joining a new company as a leader by focusing on the four pillars of effective onboarding: business orientation, stakeholder connection, alignment of expectations and cultural adaption",
    id: "23",
    locationInSource: "Chapter 1. Prepare Yourself",
    name: "Things to do when joining a new company",
    rating: 8
  });

  sampleTechniques.push({
    ...shared,
    // authorTwitter: "MichaelDWatkins",
    category: constants.TECHNIQUE_CATEGORY_KEYS.BuildingCulture,

    description:
      "Spend the time to learn how your new organisation works. This chapter has some tips on how to do this systematically.",
    id: "24",
    locationInSource: "Chapter 2. Learning",
    name: "Learn how your organisation works",
    rating: 8
  });

  sampleTechniques.push({
    ...shared,
    // authorTwitter: "MichaelDWatkins",
    category: constants.TECHNIQUE_CATEGORY_KEYS.LeaershipSkills,

    description:
      "Learn how to identify the organisational maturity of various aspects of your org and learn the techniques you can apply to improve them.",
    id: "25",
    locationInSource: "Chapter 3. Match Strategy to Situation",
    name: "Identify organisational maturity",
    rating: 8
  });

  sampleTechniques.push({
    ...shared,
    // authorTwitter: "MichaelDWatkins",
    category: constants.TECHNIQUE_CATEGORY_KEYS.BuildingCulture,

    description: "Identify problematic behaviour patterns with symptoms.",
    id: "26",
    locationInSource: "Chapter 5. Secure Early Wins, table 5-1",
    name: "Identify problematic behaviour patterns",
    rating: 8
  });

  sampleTechniques.push({
    ...shared,
    // authorTwitter: "MichaelDWatkins",
    category: constants.TECHNIQUE_CATEGORY_KEYS.Hiring,

    description:
      "Learn how and when to build your team, not too quickly but don't wait too long either.",
    id: "27",
    locationInSource: "Chapter 6. Build your team",
    name: "How to make decisions",
    rating: 8
  });
  return sampleTechniques;
}
